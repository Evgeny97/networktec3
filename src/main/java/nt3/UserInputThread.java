package nt3;

import java.util.Scanner;

public class UserInputThread extends Thread {
    private Node node;

    public UserInputThread(Node node) {
        this.node = node;
    }

    @Override
    public void run() {
        super.run();
        try {
            Scanner in = new Scanner(System.in);
            while (!this.isInterrupted()) {
                String msg = in.nextLine();
                node.putUserString(msg);
            }
        } finally {
            System.out.println("Поток ввода прерван");
        }

    }
}
