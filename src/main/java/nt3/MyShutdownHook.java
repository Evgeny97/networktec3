package nt3;
//Created by Evgeny on 07.10.2017.

public class MyShutdownHook extends Thread {
    Node node;

    public MyShutdownHook(Node node) {
        this.node = node;
    }

    @Override
    public void run() {
        System.out.println("Hook started");
        node.onExit();
    }
}
