package nt3.connections;

import nt3.messages.Message;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.LinkedList;

public class Parent implements Connection {
    private LinkedList<Message> sentMessages = new LinkedList<>();
    private InetAddress inetAddress;
    private int port;

    public Parent(InetAddress inetAddress, int port) {
        this.inetAddress = inetAddress;
        this.port = port;
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public int getPort() {
        return port;
    }

    private void addSentMessage(Message message) {
        while (sentMessages.size() >= 15) {
            sentMessages.removeFirst();
        }
        sentMessages.addLast(message);
    }

    public void sendMessage(Message message, DatagramSocket datagramSocket) {
        try {
            datagramSocket.send(message.getPacket());
            addSentMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

