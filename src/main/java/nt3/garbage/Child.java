package nt3.connections;

import nt3.connections.Connection;

import java.net.InetAddress;

public class Child implements Connection {
    private InetAddress inetAddress;
    private int port;

    public Child(InetAddress inetAddress, int port) {
        this.inetAddress = inetAddress;
        this.port = port;
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public int getPort() {
        return port;
    }
}
