package nt3.connections;

import nt3.Node;
import nt3.messages.*;
import nt3.pairs.PairIpPort;
import nt3.pairs.PairMessageSendTime;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.UUID;

public class Connection {
    ConnectionStatus connectionStatus = ConnectionStatus.Connected;
    private LinkedList<PairMessageSendTime> sentMessages = new LinkedList<>();
    private LinkedList<UUID> receivedUUIDs = new LinkedList<>();
    private InetAddress inetAddress;
    private int port;
    private Node node;
    private UUID uuidForWaitedDisconnectMessage = null;


    public Connection(InetAddress inetAddress, int port, Node node) {
        this.inetAddress = inetAddress;
        this.port = port;
        this.node = node;
    }

    public boolean isConnected() {
        return connectionStatus != ConnectionStatus.Disconnected;
    }

    public void sendLeaveMessage(){
        if (connectionStatus == ConnectionStatus.Connected){
            LeaveMessage leaveMessage = new LeaveMessage();
            sendMessage(leaveMessage);
            uuidForWaitedDisconnectMessage = leaveMessage.getUuid();
            connectionStatus = ConnectionStatus.WaitingAnswerForDisconnect;
        }
    }

    public void sendNewParentMessage(Connection parent) {
        if (connectionStatus == ConnectionStatus.Connected){
            NewParentMessage newParentMessage = new NewParentMessage(parent);
            sendMessage(newParentMessage);
            uuidForWaitedDisconnectMessage = newParentMessage.getUuid();
            connectionStatus = ConnectionStatus.WaitingAnswerForDisconnect;
        }
    }

    public InetAddress getAddress() {
        return inetAddress;
    }

    public int getPort() {
        return port;
    }

    private void addSentMessage(Message message) {
        while (sentMessages.size() >= 15) {
            sentMessages.removeFirst();
        }
        sentMessages.addLast(new PairMessageSendTime(message, System.currentTimeMillis()));
    }

    private void addReceivedUUID(UUID uuid) {
        while (receivedUUIDs.size() >= 50) {
            receivedUUIDs.removeFirst();
        }
        receivedUUIDs.addLast(uuid);
    }

    public void resendMessages(long currTime) {
        for (PairMessageSendTime pairMessageSendTime : sentMessages) {
            if (pairMessageSendTime.needResend(currTime)) {
                sendMessage(pairMessageSendTime.getMessage());
                System.out.println("Переотправка сообщения");
                pairMessageSendTime.setSendTime(currTime);
            }
        }
    }

    public void sendMessage(Message message) {
        try {
            node.getSocket().send(new DatagramPacket(message.getBuffer(), message.getBuffer().length, inetAddress, port));
            if (!sentMessages.contains(new PairMessageSendTime(message, 0)) && message.getBuffer()[0] != 0) {
                addSentMessage(message);
                System.out.println("Добавленно сообщение в очередь отправки");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendAnswer(DatagramPacket packet) {
        try {
            AnswerMessage answerMessage = new AnswerMessage(Message.getMessageUUIDFromPacket(packet));
            sendMessage(answerMessage);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    public void receivePacket(DatagramPacket packet) {
        try {
            UUID messageUUID = Message.getMessageUUIDFromPacket(packet);
            if (!receivedUUIDs.contains(messageUUID)) {
                if (packet.getData()[0] == 0) {
                    //addReceivedUUID(messageUUID);
                    ListIterator<PairMessageSendTime> iterator = sentMessages.listIterator();
                    while (iterator.hasNext()) {
                        if (messageUUID.equals(iterator.next().getMessage().getUuid())) {
                            iterator.remove();
                            System.out.println("Сообщение удалено из очереди отправки");
                            break;
                        }
                    }
                    if(connectionStatus == ConnectionStatus.WaitingAnswerForDisconnect){
                        if(messageUUID.equals(uuidForWaitedDisconnectMessage)){
                            connectionStatus = ConnectionStatus.Disconnected;
                            node.removeConnection(new PairIpPort(inetAddress,port));
                        }
                    }
                } else if (packet.getData()[0] == 1) {
                    sendAnswer(packet);
                } else if (packet.getData()[0] == 2) {
                    if (!receivedUUIDs.contains(messageUUID)) {
                        addReceivedUUID(messageUUID);
                        StringMessage stringMessage = new StringMessage(packet.getData());
                        System.out.println(stringMessage.getNodeName() + ": " + stringMessage.getMessageString());
                        node.sendStringMessages(stringMessage, this);
                    }
                    sendAnswer(packet);
                } else if (packet.getData()[0] == 3) {
                    if (!receivedUUIDs.contains(messageUUID)) {
                        addReceivedUUID(messageUUID);
                        try {
                            NewParentMessage newParentMessage = new NewParentMessage(packet.getData(), node);
                            node.setNewParent(newParentMessage.getParent());
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        }
                    }
                    sendAnswer(packet);
                } else if (packet.getData()[0] == 4) {
                    if (!receivedUUIDs.contains(messageUUID)) {
                        addReceivedUUID(messageUUID);
                        node.removeConnection(new PairIpPort(packet.getAddress(), packet.getPort()));
                    }
                    sendAnswer(packet);
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private enum ConnectionStatus {
        Connected, WaitingAnswerForDisconnect, Disconnected
    }
}

