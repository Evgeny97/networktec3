package nt3.messages;
//Created by Evgeny on 07.10.2017.

import nt3.Node;
import nt3.connections.Connection;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class NewParentMessage extends Message {
    private Connection parent;

    public NewParentMessage(Connection parent) {
        this.parent = parent;
        ByteBuffer byteBuffer = null;
        if (parent != null) {
            byteBuffer = ByteBuffer.allocate(17 + 9);
        } else {
            byteBuffer = ByteBuffer.allocate(17 + 1);
        }
        byteBuffer.put((byte) 3)
                .putLong(uuid.getMostSignificantBits())
                .putLong(uuid.getLeastSignificantBits());
        if (parent != null) {
            byteBuffer.put((byte) 1)
                    .put(parent.getAddress().getAddress())
                    .putInt(parent.getPort());
        } else {
            byteBuffer.put((byte) 0);
        }
        buffer = byteBuffer.array();

    }

    public NewParentMessage(byte[] byteData, Node node) throws UnknownHostException {
        buffer = byteData;
        Connection newParent = null;
        if (byteData[17] == 1) {
            ByteBuffer byteBuffer = ByteBuffer.wrap(buffer, 18, 8);
            byte[] inetAddressInByteArray = new byte[4];
            byteBuffer.get(inetAddressInByteArray);
            newParent = new Connection(InetAddress.getByAddress(inetAddressInByteArray), byteBuffer.getInt(), node);
        }
        this.parent = newParent;
    }

    public Connection getParent() {
        return parent;
    }
}
