package nt3.messages;

import java.nio.ByteBuffer;
import java.util.UUID;

public class AnswerMessage extends Message {

    public AnswerMessage(UUID msgUuid) {
        super(msgUuid);
        buffer = ByteBuffer.allocate(17).put((byte) 0)
                .putLong(uuid.getMostSignificantBits()).putLong(uuid.getLeastSignificantBits()).array();
    }
}
