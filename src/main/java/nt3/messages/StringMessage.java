package nt3.messages;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class StringMessage extends Message {
    private String nodeName;
    private String messageString;

    public StringMessage(String nodeName, String messageString) {
        init(nodeName, messageString);
    }

    public StringMessage(String nodeName, String messageString, UUID uuid) {
        super(uuid);
        init(nodeName, messageString);
    }

    public StringMessage(byte[] byteData) {
        buffer = byteData;
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.get();
        uuid = new UUID(byteBuffer.getLong(),byteBuffer.getLong());
        int nodeNameLength = byteBuffer.getInt();
        int messageStringLength = byteBuffer.getInt();
        byte[] nodeNameInByteArray = new byte[nodeNameLength];
        byte[] messageStringInByteArray = new byte[messageStringLength];
        byteBuffer.get(nodeNameInByteArray);
        byteBuffer.get(messageStringInByteArray);
        nodeName = new String(nodeNameInByteArray,StandardCharsets.UTF_8);
        messageString = new String(messageStringInByteArray,StandardCharsets.UTF_8);
    }

    private void init(String nodeName, String messageString) {
        this.nodeName = nodeName;
        this.messageString = messageString;
        byte[] messageStringInByteArray = messageString.getBytes(StandardCharsets.UTF_8);
        byte[] nodeNameInByteArray = nodeName.getBytes(StandardCharsets.UTF_8);
        buffer = ByteBuffer.allocate(17 + 8 + nodeNameInByteArray.length + messageStringInByteArray.length)
                .put((byte) 2)
                .putLong(uuid.getMostSignificantBits())
                .putLong(uuid.getLeastSignificantBits())
                .putInt(nodeNameInByteArray.length)
                .putInt(messageStringInByteArray.length)
                .put(nodeNameInByteArray)
                .put(messageStringInByteArray)
                .array();
    }

    public String getNodeName() {
        return nodeName;
    }

    public String getMessageString() {
        return messageString;
    }
}
