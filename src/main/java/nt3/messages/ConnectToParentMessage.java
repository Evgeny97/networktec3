package nt3.messages;


import java.nio.ByteBuffer;

public class ConnectToParentMessage extends Message {
    public ConnectToParentMessage() {
        buffer = ByteBuffer.allocate(17).put((byte) 1)
                .putLong(uuid.getMostSignificantBits()).putLong(uuid.getLeastSignificantBits()).array();
    }
}
