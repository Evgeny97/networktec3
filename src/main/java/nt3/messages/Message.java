package nt3.messages;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.util.UUID;

public abstract class Message {
    protected UUID uuid;
    protected byte[] buffer;
    //private long sendTime = -1;

    public Message() {
        this.uuid = UUID.randomUUID();
    }

    protected Message(UUID uuid) {
        this.uuid = uuid;
    }

    public static UUID getMessageUUIDFromPacket(DatagramPacket packet) throws IllegalArgumentException {
        if (packet.getData().length >= 17) {
            ByteBuffer buffer = ByteBuffer.wrap(packet.getData(), 1, 16);
            return new UUID(buffer.getLong(), buffer.getLong());
        }
        throw new IllegalArgumentException("Packet data is too short");
    }

    public UUID getUuid() {
        return uuid;
    }

    public byte[] getBuffer() {
        return buffer;
    }
}
