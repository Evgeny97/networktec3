package nt3.messages;
//Created by Evgeny on 07.10.2017.

import java.nio.ByteBuffer;

public class LeaveMessage extends Message {
    public LeaveMessage() {
        buffer = ByteBuffer.allocate(17).put((byte) 4)
                .putLong(uuid.getMostSignificantBits()).putLong(uuid.getLeastSignificantBits()).array();
    }
}
