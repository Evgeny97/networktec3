package nt3.pairs;
//Created by Evgeny on 07.10.2017.

import nt3.messages.Message;

public class PairMessageSendTime {
    private Message message;
    private long sendTime;

    public PairMessageSendTime(Message message, long sendTime) {
        this.message = message;
        this.sendTime = sendTime;
    }

    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }

    public Message getMessage() {
        return message;
    }

    public boolean needResend(long currentTime) {
        return currentTime - sendTime > 1000;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PairMessageSendTime that = (PairMessageSendTime) o;

        return message != null ? message.getUuid().equals(that.message.getUuid()) : that.message == null;
    }

    @Override
    public int hashCode() {
        return message != null ? message.getUuid().hashCode() : 0;
    }
}
