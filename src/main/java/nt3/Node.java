package nt3;

import com.sun.istack.internal.Nullable;
import nt3.connections.Connection;
import nt3.messages.ConnectToParentMessage;
import nt3.messages.StringMessage;
import nt3.pairs.PairIpPort;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;


public class Node {
    private final LinkedList<String> userStrings = new LinkedList<>();
    private final UserInputThread userInputThread;
    private final Random random = new Random();
    private String nodeName;
    private HashMap<PairIpPort, Connection> connections = new HashMap<>();
    private Connection parent = null;
    private MyShutdownHook myShutdownHook = new MyShutdownHook(this);
    private int maxSocketTimeout = 250;
    private DatagramSocket datagramSocket;
    private boolean active = false;
    private int lostPercent;

    public Node(String nodeName, int port, InetAddress parentAddress, int parentPort, int lostPercent) {
        this(nodeName, port, lostPercent);
        this.parent = new Connection(parentAddress, parentPort, this);
    }

    public Node(String nodeName, int port, int lostPercent) {
        try {
            datagramSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        userInputThread = new UserInputThread(this);
        this.nodeName = nodeName;
        this.lostPercent = lostPercent;
    }

    public String getNodeName() {
        return nodeName;
    }

    public DatagramSocket getSocket() {
        return datagramSocket;
    }

    public void putUserString(String msg) {
        synchronized (userStrings) {
            userStrings.addLast(msg);
        }
    }

    public void onExit() {
        if (active) {
            active = false;
            userInputThread.interrupt();
            if (datagramSocket != null && !datagramSocket.isClosed()) {
                try {
                    sendNewParentsToAllChild();
                    if (parent != null) {
                        sendLeaveMessageToParent();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendLeaveMessageToParent() throws IOException {
        parent.sendLeaveMessage();
        int attempts = 0;
        while (attempts < 5 && parent != null) {
            attempts++;
            startReceivingMessages();
            resendAllConnectionMessages();
        }
        if (parent == null) {
            System.out.println("успешно ливнуто");
            System.exit(0);
        }
    }

    private void startReceivingMessages() throws IOException {
        long startTime = System.currentTimeMillis();
        int timeout;
        while ((timeout = (int) (startTime + maxSocketTimeout - System.currentTimeMillis())) > 0) {
            try {
                receivePacket(timeout);
            } catch (SocketTimeoutException e) {
                break;
            }
        }
    }

    private void sendNewParentsToAllChild() throws IOException {
        if (parent != null) {
            for (Connection connection : connections.values()) {
                if (connection != parent) {
                    connection.sendNewParentMessage(parent);
                }
            }
        } else {
            if (!connections.isEmpty()) {
                Iterator<Connection> iterator = connections.values().iterator();
                Connection newParent = iterator.next();
                newParent.sendNewParentMessage(null);
                while (iterator.hasNext()) {
                    iterator.next().sendNewParentMessage(newParent);
                }
            }
        }
        int attempts = 0;
        while (attempts < 5 && connections.size() > ((parent == null) ? 0 : 1)) {
            attempts++;
            startReceivingMessages();
            resendAllConnectionMessages();
        }
    }

    private void resendAllConnectionMessages() {
        long currTime = System.currentTimeMillis();
        for (Connection connection : connections.values()) {
            connection.resendMessages(currTime);
        }
    }


    public void removeConnection(PairIpPort pairIpPort) {
        Connection connection = connections.remove(pairIpPort);
        if (connection == parent) {
            parent = null;
        }
    }

    public void setNewParent(Connection newParent) {
        connections.remove(new PairIpPort(parent.getAddress(), parent.getPort()));
        parent = newParent;
        if (parent != null) {
            connectToParent();
        }
    }

    public void startWork() {
        active = true;
        Runtime.getRuntime().addShutdownHook(myShutdownHook);
        System.out.println("Node started");
        try {
            if (parent != null) {
                connectToParent();
            }
            userInputThread.start();
            while (active) {
                startReceivingMessages();
                workWithUserStrings();
                resendAllConnectionMessages();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void workWithUserStrings() {
        synchronized (userStrings) {
            while (!userStrings.isEmpty()) {
                String msg = userStrings.removeFirst();
                if (!msg.equals("exit")) {
                    StringMessage stringMessage = new StringMessage(nodeName, msg);
                    sendStringMessages(stringMessage, null);
                } else {
                    onExit();
                }
            }
        }
    }

    private void receivePacket(int timeout) throws IOException {
        byte[] receiveBuffer = new byte[1500];
        DatagramPacket receivedPacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
        datagramSocket.setSoTimeout(timeout);
        datagramSocket.receive(receivedPacket);
        if (random.nextInt(100) > lostPercent) {
            Connection newConnection;
            if (null != (newConnection = connections.get(new PairIpPort(receivedPacket.getAddress(), receivedPacket.getPort())))) {
                newConnection.receivePacket(receivedPacket);
            } else {
                newConnectionFound(receivedPacket);
            }
        }else {
            System.out.println("типа ошибка приема");
        }
    }

    private void connectToParent() {
        ConnectToParentMessage connectToParentMessage = new ConnectToParentMessage();
        parent.sendMessage(connectToParentMessage);
        connections.put(new PairIpPort(parent.getAddress(), parent.getPort()), parent);
    }

    public void sendStringMessages(StringMessage stringMessage, @Nullable Connection sourceConnection) {
        for (Connection connection : connections.values()) {
            if (connection != sourceConnection) {
                connection.sendMessage(stringMessage);
            }
        }
    }

    private void newConnectionFound(DatagramPacket datagramPacket) {
        if (datagramPacket.getData()[0] == (byte) 1) {
            Connection newConnection = new Connection(datagramPacket.getAddress(), datagramPacket.getPort(), this);
            connections.put(new PairIpPort(datagramPacket.getAddress(), datagramPacket.getPort()), newConnection);
            newConnection.receivePacket(datagramPacket);
        } else {
            System.out.println("Cant find this socket");
        }

    }
}
