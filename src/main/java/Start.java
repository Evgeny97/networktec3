import nt3.Node;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Start {
    public static void main(String[] args) {
        Node node = null;
        if (args.length >=5) {
            try {
                node = new Node(args[0], Integer.valueOf(args[1]), InetAddress.getByName(args[2]), Integer.valueOf(args[3]), Integer.valueOf(args[4]));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        } else {
            node = new Node(args[0], Integer.valueOf(args[1]), Integer.valueOf(args[2]));
        }
        if (node != null) {
            node.startWork();
        }

        System.exit(0);
    }
}
